namespace IntSorter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IntegerArrays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeInMillis = c.Long(nullable: false),
                        SortOrder = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.IntegerArrays");
        }
    }
}
