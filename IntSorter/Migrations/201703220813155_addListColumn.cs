namespace IntSorter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addListColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IntegerArrays", "StringArray", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.IntegerArrays", "StringArray");
        }
    }
}
