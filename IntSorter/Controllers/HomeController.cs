﻿using IntSorter.Models;
using IntSorter.Models.Validation;
using System;
using System.Web.Mvc;

namespace IntSorter.Controllers
{
    public class HomeController : Controller
    {
        private IIntArrayService _manager;

        public HomeController()
        {
            _manager = new IntArrayService(new ModelStateWrapper(this.ModelState));
        }

        public HomeController(IIntArrayService _manager)
        {
            this._manager = _manager;
        }

        public ActionResult Index()
        {
            //get all items
            return View(_manager.getAll());
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult InsertNew(string numArray, string orderRbtn)
        {
            // post method is called for add new item
            // add new item to the database
            _manager.addIntArray(numArray, orderRbtn);
            return View(_manager.getAll());
        }

        [HttpPost]
        [ActionName("Delete")]
        public void DeleteArrayFromDb(string arrayId)
        {
            // method for delete item from database

            // delete item from database
            if (_manager.deleteIntArray(arrayId))
            {
                Response.Write("[{\"Status\":\"true\",\"Message\":\"\"}]");
                return;
            }
            else
            {
                string errorMess = "Some problem with deleting this item! Try it again.";
                Response.Write("[{\"Status\":\"false\",\"Message\":\"" + errorMess + "\"}]");
            }
        }
    }
}