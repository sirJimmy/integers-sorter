﻿
$(function () {

    // hide dialogs
    $('.dialog').hide();

    // create event for delete buttons
    $(".delete").click(function (event) {
        if ($('#del_dialog').is(':hidden')) {
            var actionLinkObj = $(this);
            deleteButtonfunc(event, actionLinkObj);
        }
    });
});

/*------------------------- functions for remove person  ------------------------ */
function deleteButtonfunc(even, actionLinkObj) {
    //find objects
    var dialog = $('#del_dialog');

    //var itemId = actionLinkObj.attr('itemId');

    // set position of the dialog
    var content = $('body');
    var h = content.height() / 3, w = content.width() / 2;
    var dh = dialog.height() / 2, dw = dialog.width() / 2;

    /*var newTopOffset = content.offset().top + h - dh;*/
    var newTopOffset = 50;
    var newleftOffset = content.offset().left + w - dw - 10;

    // show dialog
    dialog.show();
    dialog.offset({
        top: newTopOffset,
        left: newleftOffset
    });


    // add events to dialog buttons
    $("#del_dialog .dialog_buttons").on('click', function (e) {
        var this_button = $(this);

        //remove the event from each button where we add this
        this_button.off(e);
        var sibling = this_button.siblings();
        sibling.off();

        // test if confirmation button was click
        if (this_button.attr('id') === 'dd_confirm_btn') {
            remove_person(actionLinkObj, actionLinkObj.closest('tr'));
        }

        //hide dialog
        $('#del_dialog').hide();
        return false;
    }
    );
    return false;
}

function remove_person(actionLink, row) {
    // AJAX for call controller and delete person!!
    $.ajax({
        type: 'POST',
        url: "/Home/Delete",
        contentType: "application/x-www-form-urlencoded",
        async: false,
        data: "arrayId=" + actionLink.attr('itemId'),
        dataType: 'json',
        success: function (result) {
            if (result[0].Status === "true") {
                row.remove();
            } else {
                alert(result.Message);
            }
        },
        error: function (xhr, status, text) {
            alert(xhr.status + ' ' + text);
        },
    });
    return;
}