﻿using IntSorter.Models.DbItems;
using System.Collections.Generic;

namespace IntSorter.Models
{
    public interface IIntArrayService
    {
        IEnumerable<IntegerArray> getAll();
        bool addIntArray(string myList, string order);
        bool deleteIntArray(string id);
    }
}
