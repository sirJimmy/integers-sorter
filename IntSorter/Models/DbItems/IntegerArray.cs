﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace IntSorter.Models.DbItems
{
    public class IntegerArray
    {
        /* ------------------ Primary key ----------------------- */
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /* ------------------ Properties ----------------------- */
        // integers separated by comma
        [Required]
        public string StringArray { get; set; }

        // sorting time
        [Required]
        public long TimeInMillis { get; set; }

        // ascending / descending
        [Required]
        public String SortOrder { get; set; }

        // help method for work with String array
        [NotMapped]
        public List<int> Integers_list
        {
            get { return Array.ConvertAll(StringArray.Split(','), int.Parse).ToList(); }

            set { StringArray = string.Join(",", value); }
        }

        // help method return sorting time in seconds
        [NotMapped]
        public long TimeInSec
        {
            get
            {
                return TimeInMillis / 1000;
            }
        }
    }
}