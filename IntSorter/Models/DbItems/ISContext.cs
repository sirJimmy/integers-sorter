﻿using System.Data.Entity;

namespace IntSorter.Models.DbItems
{
    public class ISContext : DbContext
    {
        public ISContext() :
            base(nameOrConnectionString: "ConnectionString")
        {
        }

        public DbSet<IntegerArray> IntArray { get; set; }
    }
}