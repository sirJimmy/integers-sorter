﻿using System;
using System.Collections.Generic;
using IntSorter.Models.Validation;
using IntSorter.Models.DbItems;

namespace IntSorter.Models
{
    public class IntArrayService : IIntArrayService
    {
        // validator
        private IValidationDictionary _validationDictionary;

        // data access manager
        private IIntArrayManager _manager;

        public IntArrayService(IValidationDictionary validationDictionary)
            :this(validationDictionary, new IntArrayManager())
        {
        }

        public IntArrayService(IValidationDictionary validationDictionary, IIntArrayManager manager)
        {
            this._validationDictionary = validationDictionary;
            this._manager = manager;
        }

        /// <summary>
        /// Load all items from the database.
        /// </summary>
        /// <returns>All items</returns>
        public IEnumerable<IntegerArray> getAll()
        {
            return _manager.getAll();
        }

        /// <summary>
        /// Insert item to the database
        /// </summary>
        /// <param name="myList">String of integers separated by a comma</param>
        /// <param name="order">Order: ascending / descending</param>
        /// <returns>True if the item has been added, false in another case.</returns>
        public bool addIntArray(string myList, string order)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(myList))
            {
                List<int> intArray = parseStringToArray(myList);
                bool asc = order.Equals("ascending");

                if (intArray != null && intArray.Count > 0)
                {
                    string res = _manager.addIntArray(intArray, asc);
                    if (string.IsNullOrEmpty(res))
                    {
                        return true;
                    }else
                    {
                        _validationDictionary.AddError("dbError", res);
                    }
                }else
                {
                    _validationDictionary.AddError("numArray", "Wrong input. Please type just numbers separated by a comma.");
                }
            }else
            {
                _validationDictionary.AddError("numArray", "Field with numbers list is required!");
            }
            return result;
        }

        /// <summary>
        /// Delete item from the database
        /// </summary>
        /// <param name="intArrayid">Database id</param>
        /// <returns>True if the item has been deleted, false in another case.</returns>
        public bool deleteIntArray(string intArrayid)
        {
            int id;
            try
            {
                //try parse int value
                id = int.Parse(intArrayid);
            }
            catch (Exception e)
            {
                // log e
                _validationDictionary.AddError("deleteError", "Wrong Id error!");
                return false;
            }


            if (id > -1)
            {
                if (_manager.deleteIntArray(id))
                {
                    return true;
                }else
                {
                    _validationDictionary.AddError("deleteError", "Error in database!");
                }
            }
            return false;
        }

        /// <summary>
        /// Help method for parsing String to the list of the integers.
        /// </summary>
        /// <param name="s">String of the integers separated by a comma</param>
        /// <returns>List of the integers or null if parsing problem </returns>
        private List<int> parseStringToArray(string s)
        {
            List<int> result = new List<int>();
            string[] strings = s.Split(',');

            int newNum = 0;
            foreach (string num in strings)
            {
                try
                {
                    newNum = int.Parse(num);
                    result.Add(newNum);
                }
                catch (Exception e)
                {
                    //log error mess
                    result = null;
                    break;
                }
            }

            return result;
        }
    }
}