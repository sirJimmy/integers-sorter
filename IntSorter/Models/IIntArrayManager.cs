﻿using IntSorter.Models.DbItems;
using System.Collections.Generic;

namespace IntSorter.Models
{
    public interface IIntArrayManager
    {
        IEnumerable<IntegerArray> getAll();
        string addIntArray(List<int> myList, bool asc);
        bool deleteIntArray(int id);
    }
}
