﻿using IntSorter.Models.DbItems;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace IntSorter.Models
{
    public class IntArrayManager : IIntArrayManager
    {
        public IntegerArray NewIntegerArray { get; private set; }

        public IntArrayManager()
        {
            NewIntegerArray = null;
        }

        /// <summary>
        /// Load all items from the database.
        /// </summary>
        /// <returns>List of the items</returns>
        public IEnumerable<IntegerArray> getAll()
        {
            using(var dc = new ISContext())
            {
                try
                {
                    return dc.IntArray.ToList();
                }catch(Exception e)
                {
                    // log error mess here
                    return new List<IntegerArray>();
                }
            }
        }

        /// <summary>
        /// Insert item to the database 
        /// </summary>
        /// <param name="intArray">Unsorted list of the integers</param>
        /// <param name="asc">Order: true = ascending, false = descending</param>
        /// <returns></returns>
        public string addIntArray(List<int> intArray, bool asc)
        {
            string result = "";
            List<int> orderList;

            if (intArray != null)
            {
                // check the sorting time
                Stopwatch sw = Stopwatch.StartNew();
                orderList = asc ? intArray.OrderBy(x => x).ToList() : intArray.OrderByDescending(x => x).ToList();
                sw.Stop();

                using (var dc = new ISContext())
                {
                    //insert item to database
                    NewIntegerArray = new IntegerArray { Integers_list = orderList, SortOrder = asc ? "Ascending" : "Descending", TimeInMillis = sw.ElapsedMilliseconds };
                    dc.IntArray.Add(NewIntegerArray);
                    try
                    {
                        dc.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        result = "Some issue in inserting list to the database.";
                        NewIntegerArray = null;
                        // log error mess
                    }
                }
            } else
            {
                result = "Wrong input. Please type just numbers separated by a comma.";
            }
            return result;
        }

        /// <summary>
        /// Delete item from the database
        /// </summary>
        /// <param name="id">Database id</param>
        /// <returns></returns>
        public bool deleteIntArray(int id)
        {
            bool result;
            IntegerArray ia = new IntegerArray { Id = id };
            using (var dc = new ISContext())
            {
                dc.IntArray.Attach(ia);
                try
                {
                    dc.IntArray.Remove(ia);
                    dc.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                    //log error mess
                }
            }
            return result;
        }
    }
}