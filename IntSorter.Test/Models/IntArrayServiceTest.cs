﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IntSorter.Models;
using Moq;
using IntSorter.Models.Validation;
using System.Web.Mvc;

namespace IntSorter.Test.Models
{
    [TestClass]
    public class IntArrayServiceTest
    {
        private Mock<IIntArrayManager> _mockManager;
        private ModelStateDictionary _modelState;
        private IIntArrayService _service;

        [TestInitialize]
        public void Initialize()
        {
            _mockManager = new Mock<IIntArrayManager>();
            _mockManager.Setup(mr => mr.deleteIntArray(It.IsAny<int>())).Returns(true);

            _modelState = new ModelStateDictionary();
            _service = new IntArrayService(new ModelStateWrapper(_modelState), _mockManager.Object);
        }

        /* --------- Create integers array -------------- */ 
        [TestMethod]
        public void CreateIntArrayItem()
        {
            string myIntArray = "654,45,68,7,5,446,21,3";
            string myOrder = "asscending";
            bool result = _service.addIntArray(myIntArray, myOrder);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CreateIntArrayItem_Empty()
        {
            string myIntArray = string.Empty;
            string myOrder = "asscending";
            bool result = _service.addIntArray(myIntArray, myOrder);

            Assert.IsFalse(result);
            ModelError error = _modelState["numArray"].Errors[0];
            Assert.AreEqual("Field with numbers list is required!", error.ErrorMessage);
        }

        [TestMethod]
        public void CreateIntArrayItem_Wrong()
        {
            string myIntArray = "1235, 456, 13. 55, a, 456";
            string myOrder = "asscending";
            bool result = _service.addIntArray(myIntArray, myOrder);

            Assert.IsFalse(result);
            ModelError error = _modelState["numArray"].Errors[0];
            Assert.AreEqual("Wrong input. Please type just numbers separated by a comma.", error.ErrorMessage);
        }


        /* ---------------- delete integers array ---------- */
        [TestMethod]
        public void DeleteIntArrayItem()
        {
            bool result = _service.deleteIntArray("5");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DeleteIntArrayItem_WrongId()
        {
            bool result = _service.deleteIntArray("a");
            Assert.IsFalse(result);

            Assert.IsFalse(result);
            ModelError error = _modelState["deleteError"].Errors[0];
            Assert.AreEqual("Wrong Id error!", error.ErrorMessage);
        }

    }
}
